#!/usr/bin/env python3

import argparse
import json
import os
import sys
import shutil

def setup_argparser():
    parser = argparse.ArgumentParser(description='This tool creates sankey plots from LiveKraken output.')
    parser.add_argument('-i', '--infile', action='append', required=True, help='Used to list input files, can be used several times to input an ordered list of files')
    parser.add_argument('-c', '--color', action='store_true', default=False, help='Used to switch from red-green to red-blue color scheme')
    parser.add_argument('-s', '--compress', action='store_true', default=False, help='Used to "compress" unclassified nodes by only keeping a number of reads corresponding to the sum of flows from/to nodes other than unclassified.')
    parser.add_argument('-r', '--rank', choices=['species', 'genus', 'family', 'order'], default='family', help='Used to set on which level to bin the classified reads')
    parser.add_argument('-t', '--top', default=10, type=int, help='Used to determine the top x nodes to display for every cycle (plus one node serving as bin for everyting else)')
    parser.add_argument('-o', '--output', default='sankey', help='Used to set the output directory path for the html and json file')
    parser.add_argument('-m', '--names', default='names.dmp', help="Used to set the path to the names.dmp for taxonomic resolution")
    parser.add_argument('-n', '--nodes', default='nodes.dmp', help="Used to set the path to the nodes.dmp for taxonomic resolution")
    return parser

class CycleInfo:
    def __init__(self, cycle, classified, taxid, length, kmers):
        self.cycle = cycle
        self.classified = classified
        self.taxid = taxid
        self.length = length
        self.kmers = kmers


class KrakenReadClassification:
    def __init__(self, readname):
        self.readname = readname
        self.cycles = {}

    def add_cycle_info(self, cycle, classified, taxid, length, kmers):
        self.cycles[cycle] = CycleInfo(cycle, classified, taxid, length, kmers)

    def return_assignment_shift(self, cycle1, cycle2):
        return self.cycles[cycle1].taxid, self.cycles[cycle2].taxid


class KrakenReadClassificationCollection:
    def __init__(self):
        self.classifications = {}

    def extend_from_kraken_output(self, kraken_out_file_path, cycle):
        with open(kraken_out_file_path) as infile:
            for line in infile:
                classified, readname, taxid, length, kmers = \
                    KrakenReadClassificationCollection.process_kraken_output_line(line)
                try:
                    self.classifications[readname].add_cycle_info(cycle, classified, taxid, length, kmers)
                except KeyError:
                    self.classifications[readname] = KrakenReadClassification(readname)
                    self.classifications[readname].add_cycle_info(cycle, classified, taxid, length, kmers)
        return self

    def calculate_cycle_assignment_differences(self, cycle1, cycle2):
        res_dict = {}
        for readname, classification in self.classifications.items():
            shift = classification.return_assignment_shift(cycle1, cycle2)
            try:
                res_dict[shift] += 1
            except KeyError:
                res_dict[shift] = 1
        return res_dict

    def calculate_cycle_assignments(self, cycle):
        res_dict = {}
        for readname, classification in self.classifications.items():
            try:
                res_dict[classification.cycles[cycle].taxid] += 1
            except KeyError:
                res_dict[classification.cycles[cycle].taxid] = 1
        return res_dict

    def get_all_taxids_sorted_for_cycle(self, cycle):
        res_set = set()
        for readname, classification in self.classifications.items():
            res_set.add(classification.cycles[cycle].taxid)
        return sorted(list(res_set))

    def get_all_taxids_sorted_for_cycle_plus_cycle_info(self, cycle):
        return [taxid_and_cycle_transform(x, cycle) for x in self.get_all_taxids_sorted_for_cycle(cycle)]

    @staticmethod
    def process_kraken_output_line(line):
        linesplit = line.rstrip().split('\t', 4)
        classified_string = linesplit[0]
        classified = True
        if classified_string == "U":
            classified = False
        readname = linesplit[1]
        taxid = int(linesplit[2])
        length = int(linesplit[3])
        kmers = [x.split(':') for x in linesplit[4].split(' ')]
        kmers = [(x[0], int(x[1])) for x in kmers]
        return classified, readname, taxid, length, kmers

def taxid_and_cycle_transform(taxid, cycle):
    return "{}_cycle_{}".format(taxid, cycle)

def taxid_and_cycle_reverse_transform(taxid_and_cycle_string):
    str_split = taxid_and_cycle_string.split('_')
    return int(str_split[0]), int(str_split[2])

def build_taxid_dictionary(nodes_file_path):
    """
    Function that creates a TaxID dictionary {int node_id: [int node_parent_id, str node_name, str node_rank]}
    without names from the nodes.dmp file and returns this dictionary.

    :param nodes_file_path: str path to the nodes.dmp file
    :return: TaxID dictionary {int node_id: [int node_parent_id, str node_name, str node_rank]}
    """

    with open(nodes_file_path) as nodes_file:
        taxid_dict = {}
        counter = 0
        for line in nodes_file:
            counter += 1
            line_split = [x.strip() for x in line.split('|')]
            taxid_dict[int(line_split[0])] = [int(line_split[1]), '', clean_ncbi_sequence_name(line_split[2])]
            if counter % 1000 == 0:
                sys.stdout.write('Read {} nodes\r'.format(counter))
                sys.stdout.flush()

    sys.stdout.write('Read {} nodes\n'.format(len(taxid_dict)))
    sys.stdout.flush()

    # adding a node for 'unknown' organisms
    taxid_dict[0] = [1, 'unknown', 'no_rank']

    return taxid_dict

def add_names_to_taxid_dictionary(names_file_path, taxid_dict):
    """
    Function that adds the names read from names.dmp to the given TaxID dictionary which should be generated by
    build_taxid_dictionary() before.
    The function also cleans up names while parsing and keeps only numbers, letters and hypens and underscores.
    Everything else gets replaced by underscores.

    :param names_file_path: str path to names.dmp file
    :param taxid_dict:  TaxID dictionary as generated by build_taxid_dictionary()
    :return: TaxID dictionary as generated by build_taxid_dictionary() with names for all entries in names.dmp
    """

    with open(names_file_path) as names_file:
        # numbers, capital letters, other letters and - and _
        numread = 0
        numadded = 0

        for line in names_file:
            line_split = [x.strip() for x in line.split('|')]
            taxid = int(line_split[0])
            if line_split[3] == 'scientific name' and taxid in taxid_dict:  # only keep the scientific name for node
                taxid_dict[taxid][1] = clean_ncbi_sequence_name(line_split[1])
                numadded += 1

            numread += 1
            if numread % 1000 == 0:
                percentage = float(numadded)/len(taxid_dict)
                sys.stdout.write('Read {} names / Added {} ({:.2%}) names\r'.format(numread, numadded, percentage))
                sys.stdout.flush()

            if numadded == len(taxid_dict):  # if we have all names, we don't need to go further
                break

    sys.stdout.write('Read {} names / Added {} (100.00%) names\n'.format(numread, numadded))
    sys.stdout.flush()

    return taxid_dict

def clean_ncbi_sequence_name(ncbi_sequence_name):
    """
    Function that cleans up sequence descriptions as found in the NCBI sequence annotations by keeping only printable
    ASCII characters (everything else gets replaced by an underscore) as well as squeezing consecutive underscores
    together and removing leading and trailing underscores.

    :param ncbi_sequence_name: str sequence description as found in NCBI fastas
    :return: str cleaned up name
    """

    keepchars = {'!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4',
                 '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\',
                 ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
                 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}'}

    cleanname_list = []
    for character in ncbi_sequence_name.strip():
        if character in keepchars:
            cleanname_list.append(character)
        else:
            cleanname_list.append('_')
    cleanname = ''.join(cleanname_list)  # put everything back together as a string
    # squeeze several consecutive underscores into one and strip leading and trailing underscores
    return '_'.join([x for x in cleanname.split('_') if x])

def create_parent_to_children_mapping(taxid_dict):
    parent_to_children_dict = {}
    for taxid, value in taxid_dict.items():
        parent_taxid = value[0]
        try:
            parent_to_children_dict[parent_taxid].add(taxid)
        except KeyError:
            parent_to_children_dict[parent_taxid] = set()
            parent_to_children_dict[parent_taxid].add(taxid)

    return parent_to_children_dict

def get_all_children_of_taxid(taxid, parent_to_children_dict):
    try:
        all_children = parent_to_children_dict[taxid]
    except KeyError:  # our node has no children at all
        return set()
    already_visited = set()

    while True:
        difference_set = all_children.difference(already_visited)
        if not difference_set:
            break
        for child_taxid in difference_set:
            try:
                all_children = all_children.union(parent_to_children_dict[child_taxid])
            except KeyError:  # no further children
                pass
            already_visited.add(child_taxid)

    return all_children

def get_all_children_of_taxid_and_itself(taxid, parent_to_children_dict):
    return get_all_children_of_taxid(taxid, parent_to_children_dict).union({taxid})

def get_id_of_parent_on_rank(taxid, rank, taxid_dict):
    try:    # if taxid is missing from names/nodes.dmp assign these hits to root
        while True:
            # return if we arrived at family level
            if taxid_dict[taxid][2] == rank:
                return taxid
            # otherwise step upwards
            taxid = taxid_dict[taxid][0]
            # also return if we reached the root
            if taxid_dict[taxid][1] == 'root':
                return 1
    except KeyError:
        return 1
# input

parser = setup_argparser()
args = parser.parse_args(sys.argv[1:])

infiles = args.infile
nodes_file = args.nodes
names_file = args.names
rank = args.rank
how_many = args.top
out_json_path = os.path.join(args.output, 'sankey.json')
color = args.color
compress_unclassified = args.compress

# if outputfolder exists already, delete it
shutil.rmtree(args.output, ignore_errors=True)

# get this scripts location (to which relative there should be the template folder
this_scripts_directory = os.path.dirname(os.path.realpath(__file__))
shutil.copytree(os.path.join(this_scripts_directory, 'template'), args.output)


# actual program

second_color = 'green'
if color:
    second_color = 'blue'

cycles = [int(x.split('_')[-1]) for x in infiles]
taxid_dict = build_taxid_dictionary(nodes_file)
taxid_dict = add_names_to_taxid_dictionary(names_file, taxid_dict)
parent_to_child_dict = create_parent_to_children_mapping(taxid_dict)

cycle_tuples = [(cycles[x], cycles[x+1]) for x in range(len(cycles)-1)]  # tuples of consecutive cycles from above

kraken_classification_collection = KrakenReadClassificationCollection()
for index, infile in enumerate(infiles):
    kraken_classification_collection.extend_from_kraken_output(infile, cycles[index])

cycle_assignment_counts = []
for cycle in cycles:
    cycle_assignment_counts.append(kraken_classification_collection.calculate_cycle_assignments(cycle))

cycle_assignment_differences = []
for cycle1, cycle2 in cycle_tuples:
    cycle_assignment_differences.append(kraken_classification_collection.calculate_cycle_assignment_differences(cycle1, cycle2))

outfilelist = []

# bin everything in their rank bin, unclassified or other
# rewrite the assignment and assignment differences dictionaries ...
# counts per cycle
cycle_assignment_counts_binned = [{} for cycle in cycles]

for index, cycle in enumerate(cycles):
    for taxid, count in cycle_assignment_counts[index].items():
        try:
            cycle_assignment_counts_binned[index][get_id_of_parent_on_rank(taxid, rank, taxid_dict)] += count
        except KeyError:
            cycle_assignment_counts_binned[index][get_id_of_parent_on_rank(taxid, rank, taxid_dict)] = count
    outfilelist.append(open(os.path.join(args.output, "taxidcounts_{}_{}.csv".format(rank, cycle)), "w"))
    for key, value in cycle_assignment_counts_binned[index].items():
        outfilelist[-1].write("{}\t{}\n".format(key, value))
    outfilelist[-1].close()

# differences between cycle tuples
cycle_assignment_differences_binned = [{} for cycle_tuple in cycle_tuples]
for index, (cycle1, cycle2) in enumerate(cycle_tuples):
    for (taxid1, taxid2), count in cycle_assignment_differences[index].items():
        try:
            cycle_assignment_differences_binned[index][(get_id_of_parent_on_rank(taxid1, rank, taxid_dict), get_id_of_parent_on_rank(taxid2, rank, taxid_dict))] += count
        except KeyError:
            cycle_assignment_differences_binned[index][(get_id_of_parent_on_rank(taxid1, rank, taxid_dict), get_id_of_parent_on_rank(taxid2, rank, taxid_dict))] = count

# overwrite all assignments with binned assignments
cycle_assignment_counts = cycle_assignment_counts_binned
cycle_assignment_differences = cycle_assignment_differences_binned

# get top x values for changes for each round

### collecting biggest flow values
#top_cycle_assignment_differences = []
#for diff in cycle_assignment_differences:
#    sorted_diffs = sorted(diff.items(), key=lambda x: x[1], reverse=True)
#    top_cycle_assignment_differences.append(sorted_diffs[0:how_many])

# extract which nodes to display
#nodes_to_display = [[] for cycle in cycles]
#for index, diff in enumerate(top_cycle_assignment_differences):
#    for (node1, node2), value in diff:
#        nodes_to_display[index].append(node1)
#        nodes_to_display[index+1].append(node2)

#nodes_to_display = [sorted(list(set(x))) for x in nodes_to_display]
###

### collecting actual biggest nodes in each cycle
nodes_to_display = [[] for cycle in cycles]
for index, cycle_assignment in enumerate(cycle_assignment_counts):
    sorted_ass = sorted(cycle_assignment.items(), key=lambda x: x[1], reverse=True)
    nodes_to_display[index].extend([x[0] for x in sorted_ass[0:how_many]])

for i in range(len(nodes_to_display)):  # we take the -1 as rest bin
    nodes_to_display[i].insert(0, -1)

# put rest together into a big bin

# collect all names including cycle so we have each name for every cycle ... redundant redundancy
all_node_names = []
# this is to collect really all names
#for cycle in cycles:
#    all_node_names.extend(kraken_classification_collection.get_all_taxids_sorted_for_cycle_plus_cycle_info(cycle))

# this is for collecting the above filtered top nodes
for index, node_list in enumerate(nodes_to_display):
    for node in node_list:
        all_node_names.append(taxid_and_cycle_transform(node, cycles[index]))

# collect all links and their values
all_links = {}

# this is for really collecting all links
#for index, (cycle1, cycle2) in enumerate(cycle_tuples):
#    diffs_between_this_cycle_tuple = cycle_assignment_differences[index]
#    for (taxid1, taxid2), value in diffs_between_this_cycle_tuple.items():
#        all_links[(taxid_and_cycle_transform(taxid1, cycle1), taxid_and_cycle_transform(taxid2, cycle2))] = value

# this is for collecting the top links and binning the rest together
fast_lookup = [set(x) for x in nodes_to_display]
for index, (cycle1, cycle2) in enumerate(cycle_tuples):
    diffs_between_this_cycle_tuple = cycle_assignment_differences[index]
    for (taxid1, taxid2), value in diffs_between_this_cycle_tuple.items():
        # wenn beide taxids in den jeweiligen to show nodes listen ...
        if taxid1 in fast_lookup[index] and taxid2 in fast_lookup[index+1]:
            all_links[(taxid_and_cycle_transform(taxid1, cycle1), taxid_and_cycle_transform(taxid2, cycle2))] = value
        # wenn source to show
        elif taxid1 in fast_lookup[index]:
            try:
                all_links[(taxid_and_cycle_transform(taxid1, cycle1), taxid_and_cycle_transform(-1, cycle2))] += value
            except KeyError:
                all_links[(taxid_and_cycle_transform(taxid1, cycle1), taxid_and_cycle_transform(-1, cycle2))] = value
        # wenn target to show
        elif taxid2 in fast_lookup[index+1]:
            try:
                all_links[(taxid_and_cycle_transform(-1, cycle1), taxid_and_cycle_transform(taxid2, cycle2))] += value
            except KeyError:
                all_links[(taxid_and_cycle_transform(-1, cycle1), taxid_and_cycle_transform(taxid2, cycle2))] = value
        # wenn beide nicht
        else:
            try:
                all_links[(taxid_and_cycle_transform(-1, cycle1), taxid_and_cycle_transform(-1, cycle2))] += value
            except KeyError:
                all_links[(taxid_and_cycle_transform(-1, cycle1), taxid_and_cycle_transform(-1, cycle2))] = value

# remove flows between root/unclassified nodes if argument is set (keep the nodes and all flows where one node != root)
if compress_unclassified:
    entries_to_remove = []
    for node_tuple in all_links.keys():
        node1, node2 = node_tuple
        if node1.startswith('1_') and node2.startswith('1_'):
            entries_to_remove.append(node_tuple)

    for entry in entries_to_remove:
        del all_links[entry]

#  remove unclassified reads
#all_node_names = [x for x in all_node_names if not x.startswith('0_')]
#all_links = {k: v for k, v in all_links.items() if not any([taxid.startswith('0_') for taxid in k])}

# set unclassified to very small
#all_links[('0_cycle_60', '0_cycle_80')] = 1
#all_links[('0_cycle_80', '0_cycle_100')] = 1

# logarithmize link values
#for key, value in all_links.items():
#    all_links[key] = math.log(value)

# set link colors
# blue/green if same family to same family OR unclassified to anything
# red if anything to unclassified or family switch
for taxid_tuple, value in all_links.items():
    taxid1 = taxid_and_cycle_reverse_transform(taxid_tuple[0])[0]
    taxid2 = taxid_and_cycle_reverse_transform(taxid_tuple[1])[0]
    if taxid1 == 1 or taxid1 == taxid2:  # 1 is unclassified
        all_links[taxid_tuple] = [value, second_color]
    elif taxid2 == 1 or (taxid1 != taxid2):
        all_links[taxid_tuple] = [value, 'red']
    else:
        all_links[taxid_tuple] = [value, 'gray']

# transform into that shitty json format
# to json as needed for sankey plot
# structure is a dict with two keys "nodes" and "links"
# nodes value is a list of dictionaries with a single key "name" and a string value
# links value is a list of dictionaries with a "source" key (int corresponding to index in list of nodes),
# a "target" key (int corresponding to index of nodes) and
# a "value" key (float (prob int works too) designating the transfer size
all_node_names_json_format = []
for node_name in all_node_names:
    all_node_names_json_format.append({'name': node_name})

all_links_json_format = []
for all_node_name_tuple, value in sorted(all_links.items()):
    source_name = all_node_name_tuple[0]
    target_name = all_node_name_tuple[1]
    source_index = all_node_names.index(source_name)
    target_index = all_node_names.index(target_name)
    all_links_json_format.append({'source': source_index, 'target': target_index, 'value': value[0], 'color': value[1]})

# change names to readable stuff after creating the structure
for node_dict in all_node_names_json_format:
    try:
        node_dict['name'] = taxid_dict[taxid_and_cycle_reverse_transform(node_dict['name'])[0]][1]
    except KeyError:  # only the key -1 should not be present
        node_dict['name'] = 'Other'
    if node_dict['name'] == 'root':  # rename root to Unclassified
        node_dict['name'] = 'Unclassified'

json_dict = {'nodes': all_node_names_json_format, 'links': all_links_json_format}

json.dump(json_dict, open(out_json_path, 'w'))
